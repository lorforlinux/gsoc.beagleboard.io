.. _gsoc-idea-automation-and-industrial-io:

Automation and Industrial I/O
##############################

BeagleBone gets used a lot in automation tasks, be they in industrial, building, 
home or otherwise. Software that helps enable individual hobbyists, but can bridge 
into professional automation tasks, is strongly desired.

- MikroElectronika click board manifests for the Greybus simulator, instead of device tree overlays
- learning tools like BlockyTalky3 and Makecode, but with a focus on making automation easy
- open source PLC software

.. card:: 

    :fas:`wand-sparkles;pst-color-secondary` **librobotcontrol support for BeagleBone AI/AI-64 and Robotics Cape**
    ^^^^

    - **Goal:** Complete implementation of librobotcontrol on BeagleBone AI/AI-64.
    - **Hardware Skills:** `basic wiring`_, `motors`_
    - **Software Skills:** `C`_, `Linux`_
    - **Possible Mentors:** `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, `Deepak Khatri <https://forum.beagleboard.org/u/lorforlinux>`_ 
    - **Expected Size of Project:** 175 hrs
    - **Rating:** Medium
    - **Upstream Repository:** `BeagleBoard.org / librobotcontrol · GitLab <https://openbeagle.org/beagleboard/librobotcontrol>`_
    - **References:**
        - `Robotics Control Library — BeagleBoard Documentation <https://docs.beagle.cc/projects/librobotcontrol/docs/index.html>`_
        - `Robot Control Library: Main Page <https://old.beagleboard.org/static/librobotcontrol/>`_
        - http://www.strawsondesign.com/docs/librobotcontrol/index.html

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fas:`wand-sparkles;pst-color-secondary` **Makecode Arcade for PocketBeagle GamePup**
    ^^^^

    Makecode is a framework for creating special-purpose programming experiences for 
    beginners, especially focused on computer science education. Makecode has a blockly 
    based programming environment served on the browser along with an in-browser board 
    simulator. MakeCode Arcade is a similar environment to MakeCode, but the environment 
    has code blocks that are oriented towards building games. The goal of this project 
    is to support the Makecode Arcade target for the Beaglebone boards and demonstration 
    of example games on the Pocketbeagle Gamepup Cape.

    - **Complexity:** 350 hours
    - **Goal:** Makecode target for Beaglebone boards with Breadboard simulator functionality making use of a UF2 daemon running in Beaglebone
    - **Hardware Skills:** Basic breadboard prototyping skills
    - **Software Skills:** Linux,Javascript,PXT
    - **Possible Mentors:** Andrew Henderson, Vaishnav
    - **Rating:** Medium
    - **Upstream Repository:** TBD
    - **References:** 
        - https://github.com/microsoft/pxt-arcade
        - https://github.com/microsoft/uf2-linux
        - https://beagleboard.org/capes

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`


